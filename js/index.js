var canvas = document.getElementById('canvas');
var exported = document.getElementById('export');
var meter = new FPSMeter({   
 	interval:  20,     // Update interval in milliseconds.
    smoothing: 10,      // Spike smoothing strength. 1 means no smoothing.
    show:      'fps',   // Whether to show 'fps', or 'ms' = frame duration in milliseconds.
    toggleOn:  'click', // Toggle between show 'fps' and 'ms' on this event.
    decimals:  1,       // Number of decimals in FPS number. 1 = 59.9, 2 = 59.94, ...
    maxFps:    60,      // Max expected FPS value.
    threshold: 100,     // Minimal tick reporting interval in milliseconds.

    // Meter position
    position: 'fixed', // Meter position.
    zIndex:   10,         // Meter Z index.
    left:     'auto',      // Meter left offset.
    top:      '60px',      // Meter top offset.
    right:    '5px',     // Meter right offset.
    bottom:   'auto',     // Meter bottom offset.
    margin:   '0 0 0 0',  // Meter margin. Helps with centering the counter when left: 50%;

    // Theme
    theme: 'colorful', // Meter theme. Build in: 'dark', 'light', 'transparent', 'colorful'.
    heat:  1,      // Allow themes to use coloring by FPS heat. 0 FPS = red, maxFps = green.

    // Graph
    graph:   1, // Whether to show history graph.
    history: 60 // How many history states to show in a graph.;
});

var Points = '';
var Ticker = 0;

var TempExportString = '';
var ExportString = '';

var TabWidth = 260;
var CreatedTabs = 2;

var pen;

//Positions X and Y with Mouse
var x = 0;
var y = 0; 
var LastX = 0;
var LastY = 0;
var Distance = {
	Pen: 10,
	Line: 20
};

//Color of Brush / Object
var RGB = {
	R: 100,
	G: 150,
	B: 100
}
var fillColor = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
var strokeColor = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';

//Tool Variables
var tool = 'none';
var stroke = false;
var strokeWidth = 0;
var size = 20;
var fill = true;
var end = 'round';
var join = 'round';

var Notifications = {
	exportWindow: false
};

//Mouse Events
var mouse = 'mouseup';

//Window Management
var Move = {
	colors: false,
	tools: false,
	settings: false,
	brushtools: false,
	brushproperties: false,
	aboutbar: false,
	newsbar: false
};

requestAnimationFrame(Loop);

function Loop (){
	meter.tickStart();
	GetColor();
	ChangeBrushSize();
	Update();
	meter.tick();
	requestAnimationFrame(Loop);
}

Start();

function Start(){
	ExportString = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 ' + 250 + ' ' + 250 + '">\n\t';
}

function Update(){
	TempExportString = ExportString;
	exported.innerHTML = TempExportString + '</svg>';
}

function ChangeBrushSettings(Option){
	if(Option == 'BrushFill'){
		var BrushSettingsSelect = document.getElementById("Brush-Fill");
		var Selected = BrushSettingsSelect.options[BrushSettingsSelect.selectedIndex].value;
		if(Selected == 'Fill'){
			stroke = false;
			fill = true;
			document.getElementById('color').style.fill = fillColor;
			document.getElementById('color').style.stroke = strokeColor;
		}
		if(Selected == 'No-Fill'){
			fill = false;
			stroke = true;
			fillColor = 'rgba(0, 0, 0, 0)';
			document.getElementById('color').style.stroke = strokeColor;
			document.getElementById('color').style.fill = fillColor;
		}
		if(Selected == 'Fill-Stroke'){
			fill = true;
			stroke = true;
			document.getElementById('color').style.stroke = strokeColor;
			document.getElementById('color').style.fill = fillColor;
			strokeColor = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
		}
	}
	if(Option == 'BrushEnd'){
		var BrushEndSelect = document.getElementById("Brush-End");
		var Selected = BrushEndSelect.options[BrushEndSelect.selectedIndex].value;
		if(Selected == 'Rounded'){
			end = 'round';
		}
		if(Selected == 'Block'){
			end = 'none';
		}
	}
	if(Option == 'BrushJoin'){
		var BrushJoinSelect = document.getElementById("Brush-Join");
		var Selected = BrushJoinSelect.options[BrushJoinSelect.selectedIndex].value;
		if(Selected == 'Rounded'){
			join = 'round';
		}
		if(Selected == 'Block'){
			join = 'none';
		}
	}
}

function GetColor(){
	var ColorPickerSelect = document.getElementById("Brush-Setting-Color");
	var Selected = ColorPickerSelect.options[ColorPickerSelect.selectedIndex].value;
	RGB.R = document.getElementById('r').value;
	RGB.G = document.getElementById('g').value;
	RGB.B = document.getElementById('b').value;
	document.getElementById('ro').value = RGB.R;
	document.getElementById('go').value = RGB.G;
	document.getElementById('bo').value = RGB.B;
	if(Selected == 'Fill-Color' && fill){
		document.getElementById('color').style.fill = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
		fillColor = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
	}
	if(Selected == 'Stroke-Color' && stroke){
		document.getElementById('color').style.stroke = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
		strokeColor = 'rgb(' + RGB.R + ',' + RGB.G + ',' + RGB.B + ')';
	}
}

function ToolSelection(selection){tool=selection;document.getElementById('tool').innerHTML = 'Tool : ' + tool;}

$("#canvas-mouse").mousedown(function(){mouse='mousedown';});
$("#canvas-mouse").mouseup(function(){
	mouse='mouseup';
	if(tool == 'pen'){
		document.getElementById('canvas').appendChild(pen);
		ExportString += '<polyline points="' + Points + '" ' + 'stroke="' + fillColor + '" stroke-width="' + size + '" fill="transparent"/>\n\t';
		Points = '';
	}
});

function CreateCircle() {
	var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
	circle.setAttribute("cx", x);
	circle.setAttribute("cy", y);
	circle.setAttribute("r",  size);
	circle.setAttribute("fill", fillColor);
		if(stroke){
			circle.setAttribute("stroke",  strokeColor);
		}
	document.getElementById('canvas').appendChild(circle);
	LastY = y;
	LastX = x;
	if(fill){
		ExportString += '<circle cx="' + x + '" cy="' + y + '" r="' + size + '" fill="' + fillColor + '" />\n\t';
	}
	if(stroke){
		ExportString += '<circle cx="' + x + '" cy="' + y + '" r="' + size + '" stroke="' + strokeColor + '" />\n\t';
		StringExport += '<circle cx="' + x + '" cy="' + y + '" r="' + size + '" stroke="' + strokeColor + '" />\n\t';
	}
	if(fill && stroke){
		ExportString += '<circle cx="' + x + '" cy="' + y + '" r="' + size + '" fill="' + fillColor + '" stroke="' + strokeColor + '"/>\n\t';
	}
}

function CreateLine() {
	var line = document.createElementNS("http://www.w3.org/2000/svg", "line");
	line.setAttribute("x1", LastX);
	line.setAttribute("y1", LastY);
	line.setAttribute("x2", x);
	line.setAttribute("y2", y);
	line.setAttribute('stroke', fillColor);
   	line.setAttribute('stroke-width', size);
   	line.setAttribute('stroke-linejoin', join);
	line.setAttribute('stroke-linecap', end);
	document.getElementById('canvas').appendChild(line);
	ExportString += '<line x1="' + LastX + '" y1="' + LastY + '" x2="' + x + '" y2="' + y + '" stroke="' + fillColor + '" stroke-width="' + size + '"/>\n\t';
	LastY = y;
	LastX = x;
}

function Pen(){
	pen = document.createElementNS("http://www.w3.org/2000/svg", "polyline");
	pen.setAttribute("points", Points);
	pen.setAttribute("stroke", fillColor);
	pen.setAttribute('stroke-width', size);
	pen.setAttribute('stroke-linejoin', join);
	pen.setAttribute('stroke-linecap', end);
	pen.setAttribute('fill', 'transparent');
}

function ChangeBrushSize(){
	size = document.getElementById('brush-size').value;
	document.getElementById('color').style.width = size + "px";
	document.getElementById('color').style.height = size + "px";
	document.getElementById('brushsize').innerHTML = 'Brush Size : ' + size + 'px';
}

function ChangeCanvasSize(){
	var Width = document.getElementById('width').value;
	var Height = document.getElementById('height').value;
	canvas.style.width = Width + 'px';
	canvas.style.height = Height + 'px';
}

function CreateRectangle(){
	var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
	rect.setAttribute("x", x - size / 2);
	rect.setAttribute("y", y - size / 2);
	rect.setAttribute("width",  size);
	rect.setAttribute("height",  size);
	rect.setAttribute("fill", fillColor);
		if(stroke){
			rect.setAttribute("stroke",  strokeColor);
		}
	document.getElementById('canvas').appendChild(rect);
	if(fill){
		ExportString += '<rect x="' + x + '" y="' + y + '" width="' + size + '" height="' + size + '" fill="' + fillColor + '" />\n\t';
	}
	if(stroke){
		ExportString += '<rect x="' + x + '" y="' + y + '" width="' + size + '" height="' + size + '" stroke="' + strokeColor + '" />\n\t';
	}
	if(fill && stroke){
		ExportString += '<rect x="' + x + '" y="' + y + '" width="' + size + '" height="' + size + '" fill="' + fillColor + '" stroke="' + strokeColor + '"/>\n\t';
	}
	LastY = y;
	LastX = x;
}

$("#canvas-mouse").click(function(e){
	Draw();
});

$("#canvas-mouse").mousemove(function(e){
	x = e.pageX - this.offsetLeft;
	y = e.pageY - this.offsetTop;
	if(mouse == 'mousedown' && tool == 'pen'){
		Draw();
	}
});

function Draw(){
	switch(tool){
		case 'circle':
			CreateCircle();
			break;
		case 'rectangle':
			CreateRectangle();
			break;
		case 'line':
			if((x > LastX + Distance.Line || y > LastY + Distance.Line) || (x < LastX - Distance.Line || y < LastY - Distance.Line)){
				CreateLine();
			}
			break;
		case 'pen':
			if((x > LastX + Distance.Pen || y > LastY + Distance.Pen) || (x < LastX - Distance.Pen || y < LastY - Distance.Pen)){
				Ticker += 0.1;
				if(Ticker >= .2){
					Points += '' + (x + ',' + y + ' ');
					Ticker = 0;
					Pen();
				}
			}
			break;
		default:
			tool = 'none';
			break;
	}
} 

function Clear(){
	ExportString = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 ' + 250 + ' ' + 250 + '">\n\t';
	var c = canvas.children;
	for (var i = 0; i < c.length; i++) {
		 canvas.removeChild(c[i]);
	}
}

$("#colors").mousedown(function(){
	Move.colors = !Move.colors;
});

$("#toolbar").mousedown(function(){
	Move.tools = !Move.tools;
});

$("#settings").mousedown(function(){
	Move.settings = !Move.settings;
});

$("#brushbar").mousedown(function(){
	Move.brushtools = !Move.brushtools;
});

$("#brushpropertiesbar").mousedown(function(){
	Move.brushproperties = !Move.brushproperties;
});

$("#aboutbar").mousedown(function(){
	Move.aboutbar = !Move.aboutbar;
});
$("#newsbar").mousedown(function(){
	Move.newsbar = !Move.newsbar;
});

$('html').mousemove(function(e){
	if(Move.colors){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".color-pick").css('left', x1-10+'px');
		$(".color-pick").css('top', y1+10+'px');
	}
	if(Move.tools){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".tool-bar").css('left', x1-10+'px');
		$(".tool-bar").css('top', y1+10+'px');
	}
	if(Move.settings){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".canvas-settings").css('left', x1-10+'px');
		$(".canvas-settings").css('top', y1+10+'px');
	}
	if(Move.brushtools){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".brush-bar").css('left', x1-10+'px');
		$(".brush-bar").css('top', y1+10+'px');
	}
	if(Move.brushproperties){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".brush-properties-bar").css('left', x1-10+'px');
		$(".brush-properties-bar").css('top', y1+10+'px');
	}
	if(Move.aboutbar){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".about").css('left', x1-10+'px');
		$(".about").css('top', y1+10+'px');
	}
	if(Move.newsbar){
		x1 = e.pageX - this.offsetLeft;
		y1 = e.pageY - this.offsetTop;
		$(".news").css('left', x1-10+'px');
		$(".news").css('top', y1+10+'px');
	}
});

$('.close-export').click(function(){
	$('.cover').css('display', 'none');
	exported.innerHTML = ExportString;
});
$('.export-to').click(function(){
	$('.cover').css('display', 'block');
});

$('.close-about').click(function(){
	$('.about').css('display', 'none');
});

$('.open-about').click(function(){
	$('.about').css('display', 'block');
});

$('.close-news').click(function(){
	$('.news').css('display', 'none');
});

$('.open-news').click(function(){
	$('.news').css('display', 'block');
});

function Redirect(url){
	window.location.href = url;
}

function DownloadFile(){
	TempExportString += '</svg>';
	saveTextAs(TempExportString, document.getElementById('File-Name').value + ".svg");
}

function CreateTab(){
	var Tabs = document.getElementById('tabs');
	var Add = document.getElementById('add');
	var Tab = document.createElement("div");
	TabWidth += 220;
	Tabs.style.width = TabWidth + 'px'; 
	Tab.className = 'tab';
	Tabs.appendChild(Tab);
	var Name = document.createElement('span');
	Name.innerHTML = 'Tab ' + CreatedTabs;
	Tab.appendChild(Name);
	var Close = document.createElement('a');
	Close.className = 'tab-close';
	Tab.appendChild(Close);
	Tabs.insertBefore(Tab, Add);
	Tab.id = 'tab-' + CreatedTabs;
	CreatedTabs ++;
}